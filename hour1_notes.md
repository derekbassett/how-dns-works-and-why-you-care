# Hour 1 Notes

https://www.youtube.com/watch?v=Dxcc6ycZ73M What is the internet
https://www.youtube.com/watch?v=7NpczzIsnLU History of the internet
https://www.youtube.com/watch?v=2ZUxoi7YNgs DNS

* IETF  https://ietf.org/
* Internet Corporation for Assigned Names and Numbers (ICANN) 
  * https://www.icann.org/
* IANA Internet Assigned Numbers Authority 
* https://internethalloffame.org/inductees/jon-postel

# What Is DNS

The Domain Name System is a distributed Hierarchical database with low referential integrety.

## Distributed
    From Latin distributus, past participle of distribuere (“to divide,
    distribute”), from dis- (“apart”) + tribuere (“to give, impart”); see
    tribute. 

## Hierarchical
    From the Greek hierarkhia, "rule of a high priest", from
    hierarkhes, "president of sacred rites".  An arrangement of items
    (objects, names, values, categories, etc.) in which the items are
    represented as being "above", "below", or "at the same level as" one
    another. 

## Integrity 
    Steadfast adherence to a strict moral or ethical code.
    The state of being wholesome; unimpaired
    The quality or condition of being complete; pure

## Database 
    A collection of organized information in a regular structure 
    A software program for storing, retrieving and manipulating a database
    A combination of (1) and (2).


## Tree

The DNS delegation tree forms a hierarchy rooted at '.' 
TLD are the next layer down. com, net, tv, info and so on.
DNS servers are distributed to interested parties or their agents.
Operations of DNS servers is hierarchical via delegation.
It's turtles all the way down.

# NS and SOA records define the hierarchy

NS -> SOA

# Other record types carry details about resources on the internet.

* A records and AAAA
* PTR records
* MX records
* SRV
* LOC
* TXT

