# Hour 3 

# DNS Hierarchy

* The Silent Dot
* Hierarchy
* Delegation of Top Level Domains
* Negative Cacheing

# Demo

## The Silent Dot

```
dig . ns @one.one.one.one
```

## The output of `dig`

```
; <<>> DiG 9.10.3-P4-Ubuntu <<>> . ns @one.one.one.one
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 18910
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 13, AUTHORITY: 0, ADDITIONAL:
1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1452
;; QUESTION SECTION:
;.                              IN      NS

;; ANSWER SECTION:
.                       5308    IN      NS      c.root-servers.net.
.                       5308    IN      NS      d.root-servers.net.
.                       5308    IN      NS      e.root-servers.net.
.                       5308    IN      NS      f.root-servers.net.
.                       5308    IN      NS      g.root-servers.net.
.                       5308    IN      NS      h.root-servers.net.
.                       5308    IN      NS      i.root-servers.net.
.                       5308    IN      NS      j.root-servers.net.
.                       5308    IN      NS      k.root-servers.net.
.                       5308    IN      NS      l.root-servers.net.
.                       5308    IN      NS      m.root-servers.net.
.                       5308    IN      NS      a.root-servers.net.
.                       5308    IN      NS      b.root-servers.net.

;; Query time: 11 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Wed Jan 02 19:56:23 MST 2019
;; MSG SIZE  rcvd: 431
```

## An Early Vision

[From RFC1034](https://www.rfc-editor.org/rfc/rfc1034.txt)
```
3.4. Example name space

The following figure shows a part of the current domain name space, and
is used in many examples in this RFC.  Note that the tree is a very
small subset of the actual name space.

                                   |
                                   |
             +---------------------+------------------+
             |                     |                  |
            MIL                   EDU                ARPA
             |                     |                  |
             |                     |                  |
       +-----+-----+               |     +------+-----+-----+
       |     |     |               |     |      |           |
      BRL  NOSC  DARPA             |  IN-ADDR  SRI-NIC     ACC
                                   |
       +--------+------------------+---------------+--------+
       |        |                  |               |        |
      UCI      MIT                 |              UDEL     YALE
                |                 ISI
                |                  |
            +---+---+              |
            |       |              |
           LCS  ACHILLES  +--+-----+-----+--------+
            |             |  |     |     |        |
            XX            A  C   VAXA  VENERA Mockapetris
```

## A more modern Map of the internet

[http://www.internet-map.net/](http://www.internet-map.net/)

## The SOA Record
```
$ dig . soa @one.one.one.one

; <<>> DiG 9.10.3-P4-Ubuntu <<>> . soa @one.one.one.one
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 45767
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL:
1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1452
;; QUESTION SECTION:
;.                              IN      SOA

;; ANSWER SECTION:
.                       6541    IN      SOA     a.root-servers.net.
nstld.verisign-grs.com. 2019010201 1800 900 604800 86400

;; Query time: 8 msec
;; SERVER: 1.0.0.1#53(1.0.0.1)
;; WHEN: Wed Jan 02 19:59:13 MST 2019
;; MSG SIZE  rcvd: 103
```

## Delegation of Top level Domains

Draw a picture of the hierarchy.

`dig a.root-servers.net.`

https://www.iana.org/domains/root/db

`dig @1.1.1.1 example.com A +trace`

## Negative Cacheing

`dig DoesNotExist.fedde.us`

`dig DoesNotExist.fedde.us @$named_ip`

## Pair and share

* What is a DS record?
* What is an RRSIG record?
* Look up some non existant FQDN
* speculate on what will happen if you look up a non existant FQDN before you add it to your zone




