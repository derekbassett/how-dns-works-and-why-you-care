# Hour 1

[https://imgs.xkcd.com/comics/regular_expressions.png](https://imgs.xkcd.com/comics/regular_expressions.png)

## How DNS works

* Your use of the internet is enabled by the DNS.
* What is the DNS?
* Why a Distributed, Hierarchical, Low Referential Integrity Database?
* What is stored in the DNS?

## Demo

https://www.youtube.com/watch?v=2ZUxoi7YNgs

* Search DDG for for "What is the DNS"
* Search DDG for Jon Postel
* Search DDG for HOSTS.TXT
* Search DDG for DNS Record Types
* What's the difference between TCP and UDP?

## Pair and Share

* Do you have dig installed?
* Do you have whois installed?
* Do you have a partner?
* Do you have bind tools installed?
* Do you have tshark installed?

* Find a list of TLD
* Pick a record type and get ready to describe it to the class

## Regroup

Show me a list of all the available top level domains.

[Hour1_notes.md](hour1_notes.md)

