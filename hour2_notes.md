# Hour 2 Notes

# Name servers

We are not going to get into configuring nameservers.

* https://www.isc.org/downloads/bind/
* https://www.powerdns.com/
* http://www.thekelleys.org.uk/dnsmasq/doc.html

# DNS Management Software

This one is nifty. Opensourced software from Comcast.
* https://www.vinyldns.io/

# Public DNS servers

https://merabheja.com/free-open-source-dns-servers/

# Zones VS Domains and subzones vs subdomains

A zone is the part of the DNS hierarchy that is delegated to some 
area of administrative control.  Subzones are delegations from some higher level
zone.   This usage has gone out of style a bit.  To some degree the vocabulary 
has as much to do with the name server you are using rather than the DNS itself.