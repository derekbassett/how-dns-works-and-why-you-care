# Hour 2

[https://imgs.xkcd.com/comics/map_of_the_internet.jpg](https://imgs.xkcd.com/comics/map_of_the_internet.jpg)

## DNS Servers and Clients

* Servers
* Primary servers
* Secondary servers
* Cacheing and TTL
* Clients and Resolvers

## Demo

### Your "resolver" configuration
```
cat /etc/resolv.conf
cat /etc/nsswitch.conf
ps -ef | grep dnsmasq
```

### A convenient public DNS server.

```
host google-public-dns-a.google.com
host one.one.one.one
```

### The root name servers

```
dig . ns @8.8.8.8
```

### The Servers for a given TLD

```
dig us ns
dig us soa
```

### Information about the folks that run the `us` TLD domain.

```
whois cctld.us
```

## Watch the DNS traffic from your host.

In a terminal window:
```
tshark -T json port 53
```

Now reload a couple web pages

Lets try to understand what we are seeing:

## Drive an HTTP session "by hand".

In a new window

```
telnet google.com 80
GET / 1.0
<CR>
```

Watch the packet decode in the tshark window.

## Pair and Share

* How is your laptop's resolver configured?
* What are the root name servers for the `info` domain?
* Watch some DNS traffic
* Run an HTTP session by hand to `facebook.com`

[Hour2_notes.md](hour2_notes.md)
