# How DNS Works and Why You Care

## Overview

This course is a description of how your desktop other servers interacts
with the DNS and some examples that illustrate the structure for a better
understanding of what goes on under the covers

* How DNS Works
* DNS servers and Clients
* DNS hierarchy
* Looking Stuff up
* Two most common record types A (AAAA) and PTR
* GSLB and Round Robin

## Materials

* A reasonable laptop with some kind of Linux installed on it. Wifi is
  avaialable for your use.
* A VPS with linux or even a windows system with cygwin might work but
  I have not tested that.
* The bind-utils package. Eg the one that has `dig` and `host` in it.
* tcpdump and wireshark (tshark) installed.
* A shell command line that you are comfortable with. I use bash.
* The standard shell utilities like `less`, `cat`, `od` and so on.

## Links

* [Web Page](https://cfedde.gitlab.io/how-dns-works-and-why-you-care).
* [This project](https://gitlab.com/cfedde/how-dns-works-and-why-you-care/tree/master) on gitlab.
* [Slides](https://cfedde.gitlab.io/how-dns-works-and-why-you-care/slides.html)

## Author

[Chris Fedde](Author.md)